<?php

/**
 *  Route generates the $controller, $action, $params,
 *  then instantiates/fires a controller object's method
 */


class Route{
    
    // default route
    public $controller = 'home';
    public $action = 'makepost';

    public $params = [];

    /**
     *  converts first 2 slugs to method and action
     *  remaining path split up into parameters array
     *
     *  note: $_GET vars removed in first line to account for debugger
     *
     *  @param {string} $route (url path)
     */

    public function __construct(String $route){
        
        //clips out netbeans debug data from $route
        $route = explode('?',$route)[0];
        
        $route = strtolower($route);
        $route = preg_split('@/@', $route, NULL, PREG_SPLIT_NO_EMPTY);
        
        if( is_array($route) && ! empty($route) ){
            
            $this->controller = (string) array_shift($route);
            
            if( ! empty($route) ){
                $this->action = array_shift($route);
            }
            
            //any leftover slugs become params
            while(!empty($route)){
                $key = array_shift($route);
                $value = array_shift($route);
                if(isset($key) && isset($value)){
                    $this->params[$key] = $value;
                }
            }
        }
    }
    


    /**
     *  checks if $this->controller/$this->action controller-class/method combo is legit
     *
     *  @param {Object} $Session
     *  @fires {method} "$this->action" of {Object} "$this->controller.Controller" 
     */
    public function loadControllerFireAction($Session){
        
        $controllerName = ucfirst($this->controller). 'Controller';
        
        $controllerPath = ROOT_PATH. '/controllers/'. $controllerName. '.php';
        
        if(is_readable($controllerPath) && is_file($controllerPath)){
            
            require($controllerPath);
            
            $Controller = new $controllerName($this->params, $Session);

            if(is_callable([$Controller, $this->action])){
                return $Controller->{$this->action}();
            }
        }

            // else
        header('HTTP/1.0 404 Not Found');
        $view = new View();
        return $view->loadTemplate('home/error', ['errormsg'=>'Page not found']);  
    }           
}

<?php

/* 
 * Parent controller
 */

class Controller{
    
    public function __construct($params, $Session){
        $this->params = $params;
        $this->session = $Session;
        $this->view = new View();
    }
}
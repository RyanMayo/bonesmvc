<?php

/**
 *  Database Singlteton
 *  returns database instance, or dies w/ message
 *
 *  @note: this should have a more genralized error in production mode, also use a config file
 */

class Databaseinstance{
    
    private static $dbInstance = NULL;
    public static function getDbConnection(){
        if (Databaseinstance::$dbInstance == NULL){

            try{
                Databaseinstance::$dbInstance = new PDO('mysql:host=127.0.0.1;dbname=bonesmvc','root','');
            } 

            catch (Exception $e){
                die($e->getMessage());
            }
        }
        
        return Databaseinstance::$dbInstance;
    }
}

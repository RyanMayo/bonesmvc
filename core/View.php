<?php

/**
 *  assign correct view template, and wrap w/ header and footer
 *
 *  @param {array} $data
 *  @param {string} $route: like [path]/[file] example 'home/error'
 *  @param {bool} $contentOnly: render w/out header or footer
 */

class View{
  
    public function loadTemplate($route, $data =[], 
            $contentOnly = FALSE){
        
        $full_path = ROOT_PATH. '/views/'. $route. '.php';
        
        if (is_readable($full_path) && is_file($full_path)){

            if(!$contentOnly){
                include(ROOT_PATH. '/views/templates/global/header.php');
            }

            include($full_path);

            if(!$contentOnly){
                include(ROOT_PATH. '/views/templates/global/footer.php');
            }
            return TRUE;
        }else{
            throw new Exception('Template not found or unreadable');
            return FALSE;
        }
    }
}
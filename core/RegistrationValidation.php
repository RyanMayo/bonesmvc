<?php

/**
*	user registration validation
*/
class RegistrationValidation
{
	
	public $errormsg = NULL;
	public $countErrors = 0;

	/** 
	 *	chedk errorcount
	 *
	 *	@return {bool} good // not
	 */
	public function isGood(){
		if($this->countErrors === 0){
			return TRUE;
		}

		return FALSE;
	}


	/** 
	 *	if injected username search method not empty then fail
	 *
	 *	@param {array} $obj
	 */
	public function arrayIsEmpty($array, $msg = 'Error in array'){

		if( ! empty($array) ){

			$this->errormsg = $msg;
			$this->countErrors++;
		}

		return;
	}


	/** 
	 *	validate username
	 *
	 *	@param {string} $newUserName
	 */
	public function userName($newUserName){

		switch( trim($newUserName) ){

			case '':

				$this->errormsg = 'Name must be entered.';
				$this->countErrors++;
				return;

			case preg_match("/\\s/", $newUserName) == TRUE:

				$this->errormsg = 'Name must not have spaces';
				$this->countErrors++;
				return;

			case strlen($newUserName) < 4:

				$this->errormsg = 'Name must be more than 4 characters.';
				$this->countErrors++;
				return;

			default:
				return;
				break;
		}
		
	}



	/** 
	 *	validate password
	 *
	 *	@param {string} $password
	 *	@param {string} $secondPassword
	 */
	function userPassword($password, $secondPassword){
        switch($password){
            case '':
                $this->errormsg = 'Password must be entered.';
                $this->countErrors++;
                return;

            case preg_match("/\\s/", $password) == TRUE:
                $this->errormsg = 'Password must not have spaces';
                $this->countErrors++;
                return;

            case strlen($password) < 8:
                $this->errormsg = 'Password must be more than 8 characters.';
                $this->countErrors++;
                return;

            case $password != $secondPassword:
            	$this->errormsg = 'Password & Verify Password do not match.';
            	$this->countErrors++;
            	return;

            default:
				return;
                break;
        }
    }
}
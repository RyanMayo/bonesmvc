<?php

class Session{
    
    /**
    * if $_SESSION['userID'] is set by login or register then the
    * userID is used through getUserById to set users db data into
    * the $_SESSION superglobal
    *
    * or
    * 
    * if $_SESSION['userID'] is not set but there is a
    * $_COOKIE['remember_me'] with on the user-side the user's db
    * data is set by a combination of getRecord  and getUserByName
    * 
    */
    
    public function __construct(){
        session_start();
        session_regenerate_id();
        $user = new User();
        
        
        
        if(isset($_SESSION['userID'])){ 
            $userID = $_SESSION['userID'];
        }
        
        if(isset($_COOKIE['remember_me'])){  
            $rem_me = $_COOKIE['remember_me'];
        }
        
        
        if(isset($userID)){
    
            $_SESSION['user'] = $user->getUserById($userID);
        }else{
    
            // use rememberme cookie to put username & id into session
            if(isset($rem_me)){ 
                $remember = new Rememberme();
                $user_name = $remember->getRecord();
                if($user_name){
                    $user_data = $user->getUserByName($user_name);
                    $_SESSION['userID'] = $user_data['id'];
                    $_SESSION['user'] = $user_data;
                    
                    $remember->deleteRecord();
                    $token = $remember->createRecord($user_data['user_name']);
                    setcookie('remember_me', $user_data['user_name']. ':'. $token, time()+2592000, '/');
                }else{

                    if( $_SERVER['REQUEST_URI'] != '/user/logout' ){
                        header('Location:'.WEB_PATH. '/user/logout');
                        die(); 
                    }
                }
            }
        }
        
        session_write_close();
    }
    
    
    /**
     * Session setter
     *
     *  @param $key
     *  @param $value
     *
     *  @note this should probably return true / false
     */
    public function setData($key, $value){
        session_start();
        $_SESSION[$key] = $value;
        session_write_close();
    }
    
    
    /**
     *  session getter
     *
     *  @param $key
     *  @return $value || false
     */
    public function getData($key){
        session_start();
        session_write_close();
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
        }else{
            return FALSE;
        }
    }
    
    /**
     *  session destroyer
     *
     *  @param $_SESSION is reset
     */
    public function destroySession(){
        
        session_start();
        $_SESSION = [];
        
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        session_destroy();
        session_write_close();
    }
}
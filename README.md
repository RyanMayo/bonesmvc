# bonesMVC #

A light-weight cache-less micro MVC.

### What is this repository for? ###

* bonesMVC is a straight-forward microMVC currently set up as a simple blog system.  
* Version 1.0

### Setup: ###

* only works in mysql/mariaDB
* needs php 5.5+

just:

1. download
2. create a database named bonesmvc
3. import included database into database bonesmvc
4. create a user with crud permissions for bonesmvc database.
5. update core/Databaseinstance.php to reflect user name/password
6. when database is ready update core/Databaseinstance.php to reflect your new database

### Routes ###

* [root]/user/register
* [root]/user/login
* [root]/user/logout

* [root]/home/makepost
* [root]/home/updatepost/id/[post id number]

* [root]/post/getallposts/id/[user id]
* [root]/post/getallposts/name/[user name]
* [root]/post/getallposts

next 2 return posts in sets of 10

* [root]/post/getsomeposts
* [root]/post/getsomeposts/id/[user id]
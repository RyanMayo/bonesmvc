<?php

//Index/Bootstrap
//http://localhost/bonesmvc/index.php

/* mysql:
 * http://localhost/xampp/
 * 
 * current actions:
 * http://bonesmvc.local/post/getallposts/id/1
 * http://bonesmvc.local/post/getallposts
 * http://bonesmvc.local/post/getpost/id/1
 * http://bonesmvc.local/home/makepost
 * http://bonesmvc.local/user/profile/id/2
 * http://bonesmvc.local/user/register
 * http://bonesmvc.local/home/updatepost/id/10
 * http://bonesmvc.local/post/getsomeposts/id/2
 * http://bonesmvc.local/post/getsomeposts
 * 
 * Needs done checklist:
 *  File posting (all)
 *      +Validation
 X  Session
 X  Cookie
 X  getSomePosts()
 X  getSomePostsBy()
 X  updatePost() 
 X      +validation
 *  User interface
 X      +login/logout
 X      +profile page
 X  Password 
 X  Controller class
 X  Template Loader (maybe)
 X  User Error handling
 *  input sanitation
 *  output sanitation
 X  fix short tags <?= whatever ?>
 *  let user edit some functions in profile
 X  fix post model line 22 that attributes all posts to user 1
 *  fix UserController/profile()
 *  Clean up for eloquence and readability:
 X      +HomeController
 X      +PostController
 X      +UserController  --just needs user/profile fixed
 *      +models/remember_me
 X      +models/post
 *      +models/user
 * 
*/

define('ROOT_PATH', realpath(__DIR__));
define('WEB_PATH', 'http://'. $_SERVER['SERVER_NAME']);

include(ROOT_PATH. '/core/Controller.php');
include(ROOT_PATH. '/core/Databaseinstance.php');
include(ROOT_PATH. '/core/Router.php');
include(ROOT_PATH. '/core/Session.php');
include(ROOT_PATH. '/core/View.php');

//autoload forgotten models
spl_autoload_register(function($forgottenclass){
    include(ROOT_PATH. '/models/'. $forgottenclass. '.php');
});

$Session = new Session();

$Router = new Route($_SERVER['REQUEST_URI']);

$Router->loadControllerFireAction($Session);
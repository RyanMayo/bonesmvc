<?php

/**
 *	Post controller handles POST requests (and accedenatlly a few GET req..)
 *
 *	@todo move GET requests to HomeController
 */

class PostController extends Controller{
    
    
    /**
     *	@object {model} Post
     *	@param {array} params
     *	@param {Object} session
     */
    public function __construct($params, $Session) {
        parent::__construct($params, $Session);
        $this->Post = new Post();
        $this->input = [];
        $this->data = [];
    }
    
    /**
     *	assure poster is logged in & insert post and user data into table
     *	
     *	@param $_POST['post']
     */

	public function insertpost(){

		$this->input['post'] = $tester = $_POST['post'];
		$this->input['user_id'] = $this->session->getData('userID');

		if( strlen(trim($tester)) == 0 ){
			return $this->view->loadTemplate('home/makepost', ['errormsg'=>'Empty Post']);
		}

		if( ! isset($this->input['user_id']) || ! is_numeric($this->input['user_id'])){
			return $this->view->loadTemplate('user/login', ['errormsg'=>'You must be logged in to post.']);
		}


		$postId = $this->Post->insertPost($this->input);
		if( $postId ){

			header('Location:/post/getpost/id/'. $postId);
			die();
		}

		return $this->view->loadTemplate('home/makepost', ['errormsg'=>'INSERT to database failed']);
	}
    
    
    /**
     *	retrieve a single post based on id
     *
     *	@param (int) $this->params['id']
     *
     *	@todo this should probably be moved to the HommeController as it's a GET request
     */
    public function getpost(){

		if( isset($this->params['id']) && is_numeric($this->params['id'])){

			$data = $this->Post->getPost($this->params['id']);

			if( is_array($data) && ! empty($data) ){
				return $this->view->loadTemplate('home/post', ['shim' => $data]);
			}
		}

			// error msg
		return $this->view->loadTemplate('home/error', ['errormsg'=>'Retrieve post failed']);

	}
    
    
    /**
     *	if there is a id / number in params get that userid's posts
     *	if not, get all posts
     *
     *	@param {int} $this->params['id'] (optional)
	 *
     * 	@note this should be replaced w/ getsomeposts() by making limit/0 return all
     *
     *	@todo this should probably be moved to the HommeController as it's a GET request
     */
    public function getallposts(){

		if(isset($this->params['id']) != NULL){
			$userid = (int) $this->params['id'];
			$this->data = $this->Post->getAllPostsBy($userid);
		}else{

			$this->data = $this->Post->getAllPosts();
		}

		if($this->data){

			return $this->view->loadTemplate('home/post', $this->data);
		}

		return $this->view->loadTemplate('home/error', ['errormsg'=>'Retrive post failed']);
		
    }
    
    
    /**
     *	get all posts, or by user: with pagination, limit & order
     *
     *	@param {int} $this->params['offset'], default 0
	 *	@param {int} $this->params['limit'] of posts returned, default 10
	 *	@param {int} $this->params['order'] 0 ascending or 1 descending (default)
	 *	@param {int || NULL} $this->params['id']',  return posts by user id', or all (default)
	 *
     *	@todo this should probably be moved to the HommeController as it's a GET request
     */
    public function getsomeposts(){
        
		$this->input['offset'] = $this->params['offset'] ?? 0;
		$this->input['limit'] = $this->params['limit'] ?? 10;
		$this->input['order'] = $this->params['order'] ?? 1;
		$this->input['user_id'] = $this->params['id'] ?? NULL;

		if( is_numeric($this->input['user_id']) ){

			$this->data = $this->Post->getSomePostsBy($this->input);
		}else{

			$this->data = $this->Post->getSomePosts($this->input);
		}
        
		if($this->data){
			$this->view->loadTemplate('home/post', $this->data);
		}

		return $this->view->loadTemplate('home/error', ['errormsg'=>'No posts returned']);
    }
    
    
    /**
     *	update a post
     *
     *	note: this doesn't redirect back to home/updatepost on error
     *	will have to move error messageing into session to fix
     *
     *	@param {str} $_POST['post']; // new post
	 *	@param {int} $this->params['id'] // post to be edited
	 *	@param {int} session['userID']
     */
    public function updatepost(){
        
		$this->input['post'] =  $_POST['post'] ?? NULL;
		$this->input['postId'] = $this->params['id'];
		$this->input['updater_id'] = $this->session->getData('userID');

		$tableData = $this->Post->getPost($this->input['postId']);

		if($tableData['user_id'] === $this->input['updater_id'] ){

			if( ! isset($this->input['post']) || $this->input['post'] === NULL ){
				$tableData['errormsg']="Empty Post";
				return $this->view->loadTemplate('home/updatepost', $tableData);
			}

			$success = $this->Post->updatePost($this->input);

			if($success){
				header('Location:'.WEB_PATH. '/post/getpost/id/'. $tableData['id']);
				die();
			}

			$tableData['errormsg']="Edit post failed";
			$this->view->loadTemplate('home/updatepost', $tableData);
		}
			
		$tableData['errormsg']="You cannot edit other people's posts";
		$this->view->loadTemplate('home/error', $tableData);
    }
}
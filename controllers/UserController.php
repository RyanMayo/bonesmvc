<?php

class UserController extends Controller{
    
    public function __construct($params, $session) {
        parent::__construct($params, $session);
        $this->input = [];
        $this->data = [];
        $this->user = new User();
        $this->remember = new Rememberme();
    }
    
    
    /**
     *	use param id or name to retrive user data and load profile
     *
     *	@param {int} this params['id']
     *	@param {string} this params['name']
     */
	public function profile(){

		if( isset($this->params['id']) ){
			$this->data = $this->user->getUserById( trim($this->params['id']) );
		}

		if( isset($this->params['name']) ){
			$this->data = $this->user->getUserByName( trim($this->params['name']) );
		}

		if( isset($this->data['id']) ){
			unset($this->data['password']);
			return $this->view->loadTemplate('user/profile', $this->data);
		}

		return $this->view->loadTemplate('home/error', ['errormsg' => 'No Profile Found']);
	}
    
    
    /**
     *	run name and password validation scripts,
     *	check username against exisitng ones
     *		if bad, reload register page w/ error message
     *		else
     *			register user, return userId, and redirect to user profile page
     *
     *	@param {string} $this->input['potential_user_name']
     *	@param {string} $this->input['potential_user_pass']
	 *	@param {string} $this->input['potential_user_pass_ver']
	 */
	public function register(){

		// $this->data = null;

		if ($_SERVER['REQUEST_METHOD'] == 'POST'){

			$this->input = $_POST;

			require(ROOT_PATH. '/core/RegistrationValidation.php');
			$valid = new RegistrationValidation();

        	$valid->userPassword(
        		$this->input['potential_user_pass'], 
        		$this->input['potential_user_pass_ver']
        	);

        	$valid->userName($this->input['potential_user_name']);

        	$valid->arrayIsEmpty(
        		$this->user->getUserByName($this->input['potential_user_name']),
        		'Username already exists'
        	);
        	

        	if( $valid->isGood() ){

        		$this->data = $this->user->registerUser($this->input);

        		if( $this->data['id'] ){

					$this->createRecordDataAndCookie();
					header('Location:'. WEB_PATH .'/user/profile/id/'. $this->data['id']);
					die();

				}else{

					$this->data['errormsg'] = 'Server Upload Error, please try again.';
				}

        	}else{

        		$this->data['errormsg'] = $valid->errormsg;
        	}
        

    		$this->data['potential_user_name'] = $this->input['potential_user_name'];
		}

		return $this->view->loadTemplate('user/register', $this->data);
	}
    
    
    /**
     *	generate matching session, remember_me table, and cookie
     */
    private function createRecordDataAndCookie(){
		$token = $this->remember->createRecord($this->data['user_name']);
		$this->session->setData('userID',$this->data['id']);
		setcookie('remember_me', $this->data['user_name']. ':'. $token, time()+2592000, '/');
    }
    
    
    /**
     *	get user data by username
     *	check against password
     *	return profile view, or error
     *
     *	@todo: this is not great, 
     *		this should use a model method that only returns false or user id
     */
	public function login(){


		if($_SERVER['REQUEST_METHOD'] == 'POST'){

			$this->input = $_POST;

			$this->data = $this->user->getUserByName(trim($this->input['user_name']));

			if($this->data['id']){
				if( password_verify($this->input['user_pass'], $this->data['password']) ){

					$this->createRecordDataAndCookie();
					header('Location:'. WEB_PATH. '/user/profile/id/'. $this->data['id'] );
					die();
				}else{
					$error = "Password does not match";
				}
			}else{
				$error='Username not found';
			}

			return $this->view->loadTemplate('user/login', ['errormsg' => $error]);
		}

		return $this->view->loadTemplate('user/login');
	}
    
    
    /**
     *	destroy session, 
     *	delete cookie record in remember_me table, 
     *	blank cookie and set to destroy, 
     *	then load logged out template
     */
	public function logout(){

		$this->session->destroySession();
		if(isset($_COOKIE['remember_me'])){

			$this->remember->deleteRecord();
		}
		setcookie('remember_me', '', time()-86400, '/');
		$this->view->loadTemplate('user/login', ['errormsg' => 'User logged out']);
	}
}
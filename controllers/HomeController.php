<?php


/**
 *	home views are standard GET requests
 *	tend to be launching points for updates or posts
 *
 *	@todo should probably be re-named GetController
 */

class HomeController extends Controller{

	public function __construct($params, $session) {

		parent::__construct($params, $session);
		$this->input = [];
	}

	/**
	 *	load the make post view
	 */
	public function makepost(){

		$this->view->loadTemplate('home/makepost');
	}

	/**
	 *	load the make updatepost view with data of post
	 */
	public function updatepost(){

		$this->input = $this->params['id'];
		$post = new Post();
		$data = $post->getPost($this->input);
		$this->view->loadTemplate('home/updatepost', $data);
	}

	/**
	 *	quick reference phpinfo
	 *	@note: delete before production
	 */
	public function info(){
		echo '<h1>home/info</h1>';
		phpinfo();
	}
}

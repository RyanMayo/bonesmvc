<?php /*views/home/post.php*/
// This displays output from models/post which must be inside a nested array.

if(isset($data['errormsg'])){
    echo '<h3>'.$data['errormsg'].'</h3>';
}


echo '<h1>views/home/post(s)</h1>';
$clicker = 1;
foreach($data as $i):
    $clicker ++;
    if($clicker % 2 == 0){
        $bgcolor = 'rgb(230,230,255)';
    }else{
        $bgcolor = 'rgb(230,255,230)';
    }
?>
    <div style="background:<?php echo $bgcolor ?>">
            <p><span style= "font-size:1.5em;font-weight:bold;" >
                <?php echo $i['user_name'] ?></span><?php echo $i['timestamp'] ?></p>
            <p><strong>Post <?php echo $i['id'] ?>):</strong>  <?php echo $i['post'] ?></p>
<?php
                if($i['file'] == NULL){
                    echo '<p>No file Attached</p>';
                }else{
                    echo '<p>'. $i['file'] .'</p>';
                }

    echo '</div>';
endforeach;

<?php

//lifted and edited from: http://solvedstack.com/questions/php-how-to-generate-a-random-unique-alphanumeric-string

class CryptoRandSecure
{
	
	/**
	 *	uses random number between 0 and 61 and then 
	 *	spits out that letter or number from the index of $codeAlphabet
	 */

	public function getToken($length){

        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        echo $max = strlen($codeAlphabet) - 1;
        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->generate(0, $max)];
        }
        return $token;
    }

	private function generate($min, $max){
       
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }
}
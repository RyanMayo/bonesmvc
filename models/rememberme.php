<?php



/**
 *  Rememberme class
 *
 *  accesses remmeberme database to: 
 *  create, retrieve & delete username:generated-code pairs 
 *  to allow for persistant cookie-based login
 *
 *  @todo deleteRecord should return TRUE || FALSE
 */

class Rememberme{
    
    protected $db = NULL;
    
    public function __construct(){
        $this->db = Databaseinstance::getDbConnection();
    }
    
    

    /**
     *  @var $user_name
     *
     *  generates token and pairs w/ $username in remember_me table
     **/
    public function createRecord($user_name){

        require ROOT_PATH.'/vendor/CryptoRandSecure.php';

        $crs = new CryptoRandSecure;
        $token = $crs->getToken(50);
        $stmnt = 'INSERT INTO remember_me (user_name, token) VALUES (?,?)';
        $query = $this->db->prepare($stmnt);
        $query->bindParam(1, $user_name, PDO::PARAM_STR);
        $query->bindParam(2, $token, PDO::PARAM_STR);
        $result = $query->execute();
        if($result){
            return $token;
        }else{
            return FALSE;
        }
    }
    
    /**
     *  retrieves and splits cookie, then uses results to
     *  check against db:remember_me for match
     *
     *  @return (string) user_name || (bool) false
     **/
    public function getRecord(){
        $keysus = ['user_name', 'token'];
        $remember = $_COOKIE['remember_me'];
        
        $remember = explode(':', $remember);
        $remember = array_combine($keysus, $remember);
        
        $stmnt = 'SELECT * FROM remember_me WHERE user_name=? AND token=?';
        $query = $this->db->prepare($stmnt);
        $query->bindParam(1, $remember['user_name'], PDO::PARAM_STR);
        $query->bindParam(2, $remember['token'], PDO::PARAM_STR);
        $worked = $query->execute();
        
        if($worked){
            return $remember['user_name'];
        }else{
            return FALSE;
        }
        
    }
    

    /**
     *  retrieve user cookie and use username:token key/value to 
     *  delete record in db:remember_me
     **/
    public function deleteRecord(){
        $keysus = ['user_name', 'token'];
        $remember_me = $_COOKIE['remember_me'];
        
        $remember_me = explode(':', $remember_me);
        $remember_me = array_combine($keysus, $remember_me);
        
        $stmnt = 'DELETE FROM remember_me WHERE user_name=? AND token=?';
        $query = $this->db->prepare($stmnt);
        $query->bindParam(1, $remember_me['user_name'], PDO::PARAM_STR);
        $query->bindParam(2, $remember_me['token'], PDO::PARAM_STR);
        $query->execute();
        
    }
}
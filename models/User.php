<?php

/**
 *  User Model
 */

class User{
    
    public function __construct(){
        $this->db = Databaseinstance::getDbConnection();
    }
    

    /**
     *  get user data by id 
     *
     *  @param {int} $userId
     *
     *  @return {array} user data
     */
    public function getUserById($userId){

        $query = $this->db->prepare('SELECT * FROM users WHERE id = :id');
        $binds = [
            ':id' => (int) $userId
        ];

        $query->execute($binds);

        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    

    /**
     *  get user by $userName
     *
     *  @param {int} $userName
     *
     *  @return {array} user data
     */
    public function getUserByName($userName){

        $query = $this->db->prepare('SELECT * FROM users WHERE user_name = :user_name');
        $binds = [
            ':user_name' => (string) $userName
        ];

        $query->execute($binds);

        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    

    /**
     *  verify if user exists by name then validate name and password
     *
     *  @param {string} $input['potential_user_name']
     *  @param {string} $input['potential_user_pass']
     *
     *	@return {string} error message || {bool} false
     *
     *	@todo: this should absolutly not be here, 
     *		this is a validation method and should exist in a validation class
     */
 /*   public function verifyUserRegData($input){
        
        $result = $this->getUserByName($input['potential_user_name']);
        if($result){
            return 'User already exists.';
        }else{
        
            switch($input['potential_user_name']){
                case '':
                    return 'Name must be entered.';
                case preg_match("/\\s/", $input['potential_user_name']) == TRUE:
                    return 'Name must not have spaces';
                case strlen($input['potential_user_name']) < 4:
                    return 'Name must be more than 4 characters.';
                default:

                    break;
            }

            switch($input['potential_user_pass']){
                case '':
                    return 'Password must be entered.';
                case preg_match("/\\s/", $input['potential_user_pass']) == TRUE:
                    return 'Password must not have spaces';
                case strlen($input['potential_user_pass']) < 8:
                    return 'Password must be more than 8 characters.';
                default:

                    break;
            }

            if($input['potential_user_pass'] != $input['potential_user_pass_ver']){
                return 'Password & Verify Password do not match.';
            }else{
                return FALSE;
            }
        }
    }
    */

    /**
     *	insert new user into users table
     *
     *	@param {string} $input['potential_user_name']
     *	@param {string} $insert['potential_user_pass']
     *
     *	@return {array} $newUserData || {bool} false
     */
    public function registerUser($input){

        $query = $this->db->prepare('INSERT INTO users (user_name, password) VALUES (:user_name, :password)');
        $binds = [
            ':user_name' => $input['potential_user_name'],
            ':password' => password_hash($input['potential_user_pass'], PASSWORD_DEFAULT)
        ];

        $isGood = $query->execute($binds);

        if($isGood){
            
            $newUserId = $this->db->lastInsertId();
            $newUserData = $this->getUserById($newUserId);
            return $newUserData;
        }

        return FALSE;
    }
}
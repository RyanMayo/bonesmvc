<?php

/**
 *	Post Model
 */

class Post{
    private $db = NULL;
    
    public function __construct(){
        $this->db = Databaseinstance::getDbConnection();
    }
    
    
    /**
     *	insert post to posts table
     *
     *	@param {string} $input['post']
	 *	@param {int} $input['user_id']
	 *
	 *	@return {int} post id that was created || {bool} false
     */
    public function insertPost($input){

    	if(! is_numeric($input['user_id'])){
    		return FALSE;
    	}

		$stmnt = "
		INSERT INTO posts 
			(post, user_id) 
		VALUES 
			(:post, :userId)
		";

		$query = $this->db->prepare($stmnt);
		$binds = [
			':post' => $input['post'],
			':userId' => $input['user_id']
		];
		$result = $query->execute($binds);
		if($result){
			return $this->db->lastInsertId();
		}else{
			return FALSE;
		}
	}
    
    
    /**
     *	get a post by id
     *
     *	@param $postId
     *
     *	@return {array} post data || {bool} false
     */
    
	public function getPost($postId){

		if(! is_numeric($postId)){
    		return FALSE;
    	}

		$stmnt = "
		SELECT 
			posts.*, 
			users.user_name 
		FROM posts 
		INNER JOIN users 
			ON users.id = posts.user_id 
		WHERE posts.id = :postId
		";
		$query = $this->db->prepare($stmnt);
		$binds = [
			':postId' => $postId
		];

		$result = $query->execute($binds);

		if($result){
			return $query->fetch(PDO::FETCH_ASSOC);
		}

		return FALSE;
	}
    
    
    /**
     *	get all posts
     *
     *	@return {array} all posts || {bool} false
     */
    public function getAllPosts(){
		$stmnt = "
		SELECT 
			posts.*, 
			users.user_name 
		FROM posts 
		INNER JOIN users 
			ON users.id = posts.user_id
		";
		$query = $this->db->prepare($stmnt);

		$result = $query->execute();

		if($result){
			return $query->fetchAll(PDO::FETCH_ASSOC);
		}else{
			return FALSE;
		}
    }
    
    
    /**
     *	get all posts of user by $userId
     *
     *	@param {int} $userId
     *
     *	@return {array} posts || {bool} false
     */
	public function getAllPostsBy($userId){

		if( ! is_numeric($userId) ){
			return FALSE;
		}

		$stmnt = "
		SELECT 
			posts.*, 
			users.user_name 
		FROM posts 
		INNER JOIN users 
			ON users.id = posts.user_id 
		WHERE posts.user_id = :userId
		";
		$query = $this->db->prepare($stmnt);
		$binds = [
			':userId' => $userId
		];

		$result = $query->execute($binds);

		if($result){
			return $query->fetchAll(PDO::FETCH_ASSOC);
		}
			
		return FALSE;
		
	}
    

	/**
	 *	get paginated set of A USER'S posts
	 *
	 *	@param {bool} $input['order']
	 *	@param {int} $input['user_id']
	 *	@param {int} $input['offset']
	 *	@param {int} $input['limit']
	 *
	 *	@return {array} posts // {bool} false
	 */
	public function getSomePostsBy($input){

		if( boolval($input['order']) ){
			$order = 'DESC';
		}else{
			$order = 'ASC';
		}

		$stmnt = "
		SELECT 
			posts.*, 
			users.user_name 
		FROM posts 
		INNER JOIN users 
			ON users.id = posts.user_id 
		WHERE posts.user_id = ? 
		ORDER BY timestamp $order 
		LIMIT ?,? 
		";

		$query = $this->db->prepare($stmnt);

		$query->bindParam(1, $input['user_id'], PDO::PARAM_INT);
		$query->bindParam(2, $input['offset'], PDO::PARAM_INT);
		$query->bindParam(3, $input['limit'], PDO::PARAM_INT);

		$result = $query->execute();

		if($result){
			return $query->fetchAll(PDO::FETCH_ASSOC);
		}
		
		return FALSE;
	}
    
    
    /**
     *	get paginated set of ALL posts
     *
	 *	@param {bool} $input['order']
	 *	@param {int} $input['offset']
	 *	@param {int} $input['limit']
	 *
	 *	@return {array} post data || {bool} false
	 */
	public function getSomePosts($input){

		if( boolval($input['order']) ){
			$order = 'DESC';
		}else{
			$order = 'ASC';
		}

		$stmnt = "
		SELECT 
			posts.*, 
			users.user_name 
		FROM posts 
		INNER JOIN users 
			ON users.id = posts.user_id 
		ORDER BY timestamp $order 
		LIMIT ?,? 
		";
		$query= $this->db->prepare($stmnt);

		$query->bindParam(1, $input['offset'], PDO::PARAM_INT);
		$query->bindParam(2, $input['limit'], PDO::PARAM_INT);

		$result = $query->execute();

		if($result){
			return $query->fetchAll(PDO::FETCH_ASSOC);
		}

		return FALSE;
	}
    
    /**
     *	update post by postId
     *
     *	@param {string} $input['post']
     *	@param {int} $input['postId']
     *
     *	@return {int} id of post updated || {bool} false
     */
	public function updatePost($input){

		if( ! is_numeric($input['postId']) ){
			return false;
		}

		$stmnt = "
		UPDATE posts 
		SET post=:post 
		WHERE id=:id
		";

		$query = $this->db->prepare($stmnt);

		$binds = [
			':post'=> $input['post'],
			':id'=> $input['postId']
		];

		$isGood = $query->execute($binds);

		if($isGood){
			return $input['postId'];
		}

		return FALSE;
	}
}

